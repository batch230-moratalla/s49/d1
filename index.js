// console.log("dasdsd")
// fetch("https://jsonplaceholder.typicode.com/posts")
// .then((response) => response.json()
// .then((perResult) => showPosts(perResult)));

// fetch("https://jsonplaceholder.typicode.com/posts")
// .then((response) => response.json())
// .then((result) => showPosts(result));


fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((result) => {
  console.log(result)
  return showPosts(result)
})
// showPosts(result));


const showPosts = (posts) => {
  let postEntries = "";
  posts.forEach((post) => {

    postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title} </h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
    `
  });
  document.querySelector("#div-post-entries").innerHTML = postEntries
  // document.querySelector(`#div-post-entries`).innerHTML = postEntries;
}

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
  event.preventDefault();
  
  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: document.querySelector("#txt-title").value,
      body: document.querySelector("#txt-body").value,
      userId: 101
    }),
    headers: {"Content-type" : "application/json ; charset = UTF-8"}
  })
  .then(response => response.json())
  .then(result => {
    console.log(result);
    alert("Successfullyy Added.")

    document.querySelector("#txt-title").value = null;
    document.querySelector("#txt-body").value = null;
  })
})

// Edit post
const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;//"#post-title" + id
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault()

  // Update post
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    // Specify what HTTP method to be used
    method: "PUT",
    body: JSON.stringify({
      id: document.querySelector("#txt-edit-id").value,
      title: document.querySelector("#txt-edit-title").value,
      body: document.querySelector("#txt-edit-body").value,
      userId: 1
    }),
    headers: {"Content-type" : "application/json ; charset = UTF-8"}
  })
  .then((response) => response.json())
  .then(result => {
    console.log(result);
    alert("Successfully Updated");

    // Clear the input box and value / make the input box null
    document.querySelector("#txt-edit-id").value = null;
    document.querySelector("#txt-edit-title").value = null;
    document.querySelector("#txt-edit-body").value = null;

    // re-disable functionality
    document.querySelector("#btn-submit-update").setAttribute("disabled", true);
  })
})

// Delete a Post
const deletePost = (id) => {
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "DELETE" 
  })
  .then((response) => response.json())
  .then(result => {
    console.log(result);
    alert("Successfully Deleted");
    document.querySelector(`#post-${id}`).remove()
  })
}
